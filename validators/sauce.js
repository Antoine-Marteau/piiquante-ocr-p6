const { body } = require('express-validator')

const sauceFields = () => {
  return [
    body('name').isString().notEmpty().isLength({min:5}),
    body('manufacturer').isString().notEmpty(),
    body('description').isString().notEmpty(),
    body('mainPepper').isString().notEmpty(),
    body('heat').isInt().notEmpty(),
    body('userId').isString().notEmpty()
  ]
}

module.exports = {
  sauceFields
}