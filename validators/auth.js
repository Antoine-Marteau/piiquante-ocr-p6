const { body } = require('express-validator')

const authFields = () => {
  return [
    body('email').isEmail(),
    body('password').isString().notEmpty()
  ]
}

module.exports = {
  authFields
}