const express = require('express');
const router = express.Router();
const app = express();

/* MULTER */
const multer = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    let sauceData = JSON.parse(req.body.sauce);
    let fileExtension = file.mimetype.split('/')[1];
    let fileName = sauceData.name + '-' + Date.now() + '.' + fileExtension;
    cb(null, fileName.replace(/ /gm, '_'));
  }
})

var uploadPost = multer({ storage: storage });
var uploadPut = multer();

const authMiddleware = require('../middleware/auth');
const fileMiddleware = require('../middleware/fileDetector');
const formMiddleware = require('../middleware/formValidator');
const sauceFunctions = require('../controllers/sauce');
const { sauceFields } = require('../validators/sauce');

router.get('/', authMiddleware, sauceFunctions.getAllSauces);
router.get('/:id', authMiddleware, sauceFunctions.getOneSauce);
router.post('/', authMiddleware, uploadPost.single('image'), fileMiddleware, sauceFields(), formMiddleware.formValidator, sauceFunctions.createNewSauce);
router.put('/:id', authMiddleware, uploadPost.single('image'), fileMiddleware, sauceFields(), formMiddleware.formValidator, sauceFunctions.changeSauce);
router.delete('/:id', authMiddleware, sauceFunctions.deleteSauce);
router.post('/:id/like', authMiddleware, sauceFunctions.likeSauce);

module.exports = router;