const express = require('express');
const router = express.Router();

const formMiddleware = require('../middleware/formValidator');

const authFunctions = require('../controllers/auth');
const { authFields } = require('../validators/auth');

router.post('/signup', authFields(), formMiddleware.formValidator, authFunctions.signup);
router.post('/login', authFields(), formMiddleware.formValidator, authFunctions.login);

module.exports = router;