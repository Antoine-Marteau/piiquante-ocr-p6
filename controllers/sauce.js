const Sauce = require('../models/sauce');
const mongoose = require('mongoose');
const fs = require('fs')

exports.getAllSauces = (req, res, next) => {
    
    Sauce.find()
        .then(sauces => res.status(200).json(sauces))
        .catch(error => res.status(400).json({ error }));
}

exports.getOneSauce = (req, res, next) => {

    Sauce.findById(req.params.id)
    .then(sauceId => {
        res.status(200).json(sauceId);
    })
    .catch(error => res.status(400).json({ error }));
}

exports.createNewSauce = (req, res, next) => {

    let sauceData = req.body;

    const newSauce = new Sauce({
      _id: mongoose.Types.ObjectId(),
      userId: req.userId,
      name: sauceData.name,
      manufacturer: sauceData.manufacturer,
      description: sauceData.description,
      mainPepper: sauceData.mainPepper,
      imageUrl: sauceData.imageUrl,
      heat: sauceData.heat,
      likes: 0,
      dislikes: 0,
      usersLiked: [],
      usersDisliked: []
    });

    newSauce.save()
      .then(() => {
          res.status(201).json({ message: 'Objet enregistré ! 2'})
        })
      .catch(error => res.status(400).json({ error }));
}

exports.changeSauce = (req, res, next) => {

    let sauceData = req.body;

    if (sauceData.imageUrl) {
        Sauce.findById(req.params.id)
            .then(sauceId => {
                let filename = sauceId.imageUrl.split('uploads/')[1];
                fs.unlinkSync('./uploads/'+filename);
            })
            .catch(error => res.status(400).json({ error }));
    }

    Sauce.updateOne({ _id: req.params.id }, {
        name: sauceData.name,
        manufacturer: sauceData.manufacturer,
        description: sauceData.description,
        mainPepper: sauceData.mainPepper,
        imageUrl: sauceData.imageUrl,
        heat: sauceData.heat,
    })
    .then(() => {
        res.status(200).json({ message: 'Sauce modifié !'})
    })
    .catch(error => res.status(400).json({ error }));
}

exports.deleteSauce = (req, res, next) => {

    Sauce.findById(req.params.id)
        .then(sauceId => {
            let filename = sauceId.imageUrl.split('uploads/')[1];
            fs.unlinkSync('./uploads/'+filename);
        })
        .catch(error => res.status(400).json({ error }));

    Sauce.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Sauce supprimé !'}))
        .catch(error => res.status(400).json({ error }));
}

exports.likeSauce = (req, res, next) => {
    
    var likeValue = req.body.like;
    var userId = req.body.userId;

    Sauce.findById(req.params.id)
        .then(sauceData => {

            switch (likeValue) {
                
                case -1:

                    sauceData.usersDisliked.push(userId);

                    Sauce.updateOne({ _id: req.params.id }, {
                        dislikes: (sauceData.dislikes + 1),
                        usersDisliked: sauceData.usersDisliked,
                    })
                    .catch(error => res.status(400).json({ error }));

                    break;
        
                case 1:

                    sauceData.usersLiked.push(userId);

                    Sauce.updateOne({ _id: req.params.id }, {
                        likes: (sauceData.likes + 1),
                        usersLiked: sauceData.usersLiked,
                    })
                    .catch(error => res.status(400).json({ error }));

                    break;

                case 0:
                    
                    if(sauceData.usersLiked.includes(userId)){

                        let indexUserId = sauceData.usersLiked.indexOf(userId);
                        sauceData.usersLiked.splice(indexUserId, 1);

                        Sauce.updateOne({ _id: req.params.id }, {
                           likes: (sauceData.likes - 1),
                           usersLiked: sauceData.usersLiked,
                        })
                        .catch(error => res.status(400).json({ error }));

                    } else {

                        let indexUserId = sauceData.usersDisliked.indexOf(userId);
                        sauceData.usersDisliked.splice(indexUserId, 1);

                        Sauce.updateOne({ _id: req.params.id }, {
                           dislikes: (sauceData.dislikes - 1),
                           usersDisliked: sauceData.usersDisliked,
                        })
                        .catch(error => res.status(400).json({ error }));
                    }

                    break;
            
                default:
                    break;
            }

        })
        .then(() => res.status(200).json({ message: 'Likes mis à jour !'}))
        .catch(error => res.status(400).json({ error }));
}