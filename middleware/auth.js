const jwt = require('jsonwebtoken');
const User = require('../models/user');

module.exports = (req, res, next) => {

  let token = process.env.TOKEN_SECRET;
  try {
    const idToken = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(idToken, token);
    const userId = decodedToken.userId;
    req.userId = userId;

    User.findById(userId, function(err, user){
      if (user) {
        next();
      }
      if (err) {
        res.status(401).json({ error });
      }
    });

  } catch (error) {
    res.status(401).json({
     error: new Error('Invalid request!')
    });
  }
};