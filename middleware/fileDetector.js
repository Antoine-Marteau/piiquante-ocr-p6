module.exports = (req, res, next) => {

  if (req.body.sauce) {
    let sauceData = JSON.parse(req.body.sauce);
    req.body = sauceData;
  }
  if (req.file) {
    req.body.imageUrl = req.protocol + "://" + req.get('host') + "/" + req.file.destination + req.file.filename;
  }

  next();
};