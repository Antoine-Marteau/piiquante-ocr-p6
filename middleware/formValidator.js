const { validationResult } = require('express-validator');
const fs = require('fs');

exports.formValidator = (req, res, next) => {
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next()
  } else {
    if (req.body.imageUrl) {
      fs.unlinkSync('./' + req.file.destination + req.file.filename);
    }
  }
  const extractedErrors = []
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

  return res.status(422).json({
    errors: extractedErrors,
  })
}