const express = require('express');
const path = require('path');

const sauce = require('./routes/sauce');
const auth = require('./routes/auth');

const app = express();

require('dotenv').config();
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(express.static('uploads'));

mongoose.connect(process.env.MONGO_URI,
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use('/api/sauces', sauce);
app.use('/api/auth', auth);

module.exports = app;